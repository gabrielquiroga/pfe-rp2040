
#include <MessageParser.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>

using ::testing::NiceMock;

class MessageParserTest : public ::testing::Test {
protected:
};

TEST(MessageParserUTest, Builds) { auto parser = MessageParser(); }
TEST(MessageParserUTest, ParsesData) {
  auto parser = MessageParser();
  const std::string data = "{\"x\":1.23,\"o\":4.33}\n";
  auto [x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, 1.23);
  EXPECT_FLOAT_EQ(o, 4.33);
}
TEST(MessageParserUTest, ParsesNegativeData) {
  auto parser = MessageParser();
  const std::string data = "{\"x\":-4.23,\"o\":-1.33}\n";
  auto &&[x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, -4.23);
  EXPECT_FLOAT_EQ(o, -1.33);
}
TEST(MessageParserUTest, HandlesNullData) {
  auto parser = MessageParser();
  const std::string data = "{}\n";
  auto &&[x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, 0.0);
  EXPECT_FLOAT_EQ(o, 0.0);
}
TEST(MessageParserUTest, HandlesNullXData) {
  auto parser = MessageParser();
  const std::string data = "{\"o\":-1.33}\n";

  auto &&[x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, 0.0);
  EXPECT_FLOAT_EQ(o, -1.33);
}
TEST(MessageParserUTest, HandlesNullOData) {
  auto parser = MessageParser();
  const std::string data = "{\"x\":-4.23}\n";
  auto &&[x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, -4.23);
  EXPECT_FLOAT_EQ(o, 0.0);
}
TEST(MessageParserUTest, HandlesWrongOData) {
  auto parser = MessageParser();
  const std::string data = "{\"x\": ,,\n";
  auto &&[x, o] = parser.parse(data);
  EXPECT_FLOAT_EQ(x, 0.0);
  EXPECT_FLOAT_EQ(o, 0.0);
}

TEST(MessageParserUTest, EncodesData) {
  auto parser = MessageParser();
  auto message = parser.encode({1.23, 4.33});
  auto const &[x, o] = parser.parse(message);
  EXPECT_NEAR(x, 1.23, 0.01);
  EXPECT_NEAR(o, 4.33, 0.01);
}
