#include "main/tasks.hpp"
#include "testing/fff.h"
#include "gtest/gtest.h"
#include <Store.hpp>
#include <iostream>

FAKE_VOID_FUNC(notifyTask, TaskHandleID);

class StoreTest : public ::testing::Test {
protected:
  void SetUp() {
    RESET_FAKE(notifyTask);
    FFF_RESET_HISTORY();
  };
};

// store.updateCurrentRotPos(payload);
// store.updateTargets(newTarget);

TEST_F(StoreTest, ShouldBuild) {
  const auto &store = Store::instance();
  EXPECT_EQ(store.getDiffs().first, 0);
  EXPECT_EQ(store.getDiffs().second, 0);
  EXPECT_EQ(store.getCurrentPosition(), 0);
  EXPECT_EQ(store.getCurrentRotation(), 0);
};
