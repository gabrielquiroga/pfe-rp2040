#include "mocks/MPU6500Mock.hpp"
#include "proxies/timestampProxy.hpp"
#include "testing/fff.h"
#include "gtest/gtest.h"
#include <GyroProcessor.hpp>
#include <IMUDataProcessor.hpp>
#include <memory>

FAKE_VALUE_FUNC(absolute_time_t, get_absolute_time);
FAKE_VALUE_FUNC(int64_t, absolute_time_diff_us, absolute_time_t,
                absolute_time_t);

using ::testing::NiceMock;

class IMUTest : public ::testing::Test {
protected:
  void SetUp() {
    RESET_FAKE(get_absolute_time);
    RESET_FAKE(absolute_time_diff_us);
    FFF_RESET_HISTORY();
  };
};

TEST_F(IMUTest, shouldBuildGyro) {
  auto gyroProcessor = GyroProcessor::Builder().build();
}

TEST_F(IMUTest, shouldBuildIMU) {
  auto mpu = std::make_unique<NiceMock<MPU6500Mock>>();

  auto IMU = IMUDataProcessor(std::move(mpu));
}
