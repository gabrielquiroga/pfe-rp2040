#include "mocks/PinMock.hpp"
#include "testing/fff.h"
#include "gtest/gtest.h"
#include <PWM.hpp>

FAKE_VALUE_FUNC(uint, pwm_gpio_to_slice_num, uint);
FAKE_VALUE_FUNC(uint, pwm_gpio_to_channel, uint);
FAKE_VOID_FUNC(pwm_set_enabled, uint, bool);
FAKE_VOID_FUNC(pwm_set_clkdiv_int_frac, uint, uint8_t, uint8_t);
FAKE_VOID_FUNC(pwm_set_wrap, uint, uint16_t);
FAKE_VOID_FUNC(pwm_set_chan_level, uint, uint, uint16_t);

using ::testing::AtLeast;
using ::testing::NiceMock;

class PWMTest : public ::testing::Test {
protected:
  void SetUp() {
    RESET_FAKE(pwm_gpio_to_slice_num);
    RESET_FAKE(pwm_gpio_to_channel);
    RESET_FAKE(pwm_set_enabled);
    RESET_FAKE(pwm_set_clkdiv_int_frac);
    RESET_FAKE(pwm_set_wrap);
    RESET_FAKE(pwm_set_chan_level);
    FFF_RESET_HISTORY();
  };
  const int SHARED_PIN = 7;
  std::unique_ptr<NiceMock<PinMock>> pinMock =
      std::make_unique<NiceMock<PinMock>>();
};

TEST_F(PWMTest, ShouldBuild) {
  EXPECT_CALL(*pinMock, getPin()).Times(2);
  PWM pwm{std::move(pinMock)};
};
