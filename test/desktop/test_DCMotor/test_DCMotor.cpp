#include "DCMotor.hpp"
#include "mocks/PWMMock.hpp"
#include "mocks/PinMock.hpp"
#include "gtest/gtest.h"
#include <memory>

namespace DCMotorTests {

using ::testing::NiceMock;

class DCMotorTest : public ::testing::Test {
protected:
  std::unique_ptr<NiceMock<PinMock>> forwardMock =
      std::make_unique<NiceMock<PinMock>>();
  std::unique_ptr<NiceMock<PinMock>> reverseMock =
      std::make_unique<NiceMock<PinMock>>();
  std::unique_ptr<NiceMock<PWMMock>> speedMock =
      std::make_unique<NiceMock<PWMMock>>();

  std::function<DCMotor(void)> buildMotor = [=]() {
    return DCMotor(std::move(forwardMock), std::move(reverseMock),
                   std::move(speedMock));
  };
};

TEST_F(DCMotorTest, BuildsMotor) {
  auto motor = buildMotor();
  EXPECT_EQ(motor.getDirection(), true);
  EXPECT_EQ(motor.getSpeed(), 0);
};

TEST_F(DCMotorTest, StopMethodCallsSetsDutyTo0) {
  EXPECT_CALL(*speedMock, duty(0));
  auto motor = buildMotor();
  motor.stop();
}

TEST_F(DCMotorTest, ForwardsSetsRightSpeed) {
  const auto targetSpeed = 50;
  EXPECT_CALL(*forwardMock, on());
  EXPECT_CALL(*reverseMock, off());
  EXPECT_CALL(*speedMock, duty(50));

  auto motor = buildMotor();
  motor.forward(targetSpeed);

  EXPECT_EQ(motor.getDirection(), true);
  EXPECT_EQ(motor.getSpeed(), 50);
}

TEST_F(DCMotorTest, ReverseSetsRightSpeed) {
  const auto targetSpeed = 50;
  EXPECT_CALL(*forwardMock, off());
  EXPECT_CALL(*reverseMock, on());
  EXPECT_CALL(*speedMock, duty(50));

  auto motor = buildMotor();
  motor.reverse(targetSpeed);

  EXPECT_EQ(motor.getDirection(), false);
  EXPECT_EQ(motor.getSpeed(), -targetSpeed);
}

TEST_F(DCMotorTest, ForwardsClampsTo100) {
  const auto targetSpeed = 101;
  EXPECT_CALL(*forwardMock, on());
  EXPECT_CALL(*reverseMock, off());
  EXPECT_CALL(*speedMock, duty(100));

  auto motor = buildMotor();
  motor.forward(targetSpeed);

  EXPECT_EQ(motor.getDirection(), true);
  EXPECT_EQ(motor.getSpeed(), 100);
}

TEST_F(DCMotorTest, MoveCallsStop) {
  const auto targetSpeed = 0;
  EXPECT_CALL(*forwardMock, off());
  EXPECT_CALL(*reverseMock, off());
  EXPECT_CALL(*speedMock, duty(0));

  auto motor = buildMotor();
  const auto initialDirection = motor.getDirection();
  motor.move(targetSpeed);

  EXPECT_EQ(motor.getDirection(), initialDirection);
  EXPECT_EQ(motor.getSpeed(), 0);
}
TEST_F(DCMotorTest, MoveCallsForward) {
  const auto targetSpeed = 0;
  EXPECT_CALL(*forwardMock, off());
  EXPECT_CALL(*reverseMock, off());
  EXPECT_CALL(*speedMock, duty(0));

  auto motor = buildMotor();
  const auto initialDirection = motor.getDirection();
  motor.move(targetSpeed);

  EXPECT_EQ(motor.getDirection(), initialDirection);
  EXPECT_EQ(motor.getSpeed(), targetSpeed);
}
TEST_F(DCMotorTest, MoveCallsReverse) {
  const auto targetSpeed = -1;
  EXPECT_CALL(*forwardMock, off());
  EXPECT_CALL(*reverseMock, on());
  EXPECT_CALL(*speedMock, duty(1));

  auto motor = buildMotor();
  const auto initialDirection = motor.getDirection();
  motor.move(targetSpeed);

  EXPECT_EQ(motor.getDirection(), false);
  EXPECT_EQ(motor.getSpeed(), targetSpeed);
}

} // namespace DCMotorTests