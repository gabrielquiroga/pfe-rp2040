#include <DCMotor.hpp>
#include <DifferentialRobot.hpp>
#include <OpticalEncoder.hpp>
#include <Pin.hpp>
#include <Store.hpp>
#include <UART.hpp>

#include <pico/multicore.h>
#include <pico/stdlib.h>
#include <pico/util/queue.h>

#include <stdio.h>
#include <string>

#include <FreeRTOS.h>
#include <task.h>

#include "config.hpp"

#include "main/main.hpp"
#include "main/tasks.hpp"

static void vSetupStore();

int main(void) {
  // Setup Central State Store
  vSetupStore();

  // Launch Core 1 Task
  multicore_launch_core1(main_core1);

  // Setups FreeRTOS Tasks
  vSetupTasks();

  // init FreeRTOS
  vTaskStartScheduler();

  while (true) {
    // nothing to do
  };

  return 0;
};

static void vSetupStore() {
  // Setup
  auto &store = Store::instance();

  // Init Store's UART
  store.setUart(std::move(UART::Builder()
                              .withTXat(config::tx)
                              .withRXat(config::rx)
                              .withBaudrate(config::baudrate)
                              .get()));

  auto &uart = store.getUart();
  // Configure UART Interrupts

  uart.irq([&uart]() {
    uart.getDefaultHandler()();
    if (uart.hasMessages()) {
      notifyTaskFromISR(TaskHandleID::PROCESS_INCOMING_MESSAGE);
    }
  });

  auto leftEngine = DCMotor::Builder()
                        .withForwardAt(config::leftEngineForwardPin)
                        .withReverseAt(config::leftEngineReversePin)
                        .withSpeedControlAt(config::leftEngineSpeedControlPin)
                        .get();

  auto rightEngine = DCMotor::Builder()
                         .withForwardAt(config::rightEngineForwardPin)
                         .withReverseAt(config::rightEngineReversePin)
                         .withSpeedControlAt(config::rightEngineSpeedControlPin)
                         .get();

  store.setRobot(DifferentialRobot::Builder()
                     .withLeftEngine(std::move(leftEngine))
                     .withRightEngine(std::move(rightEngine))
                     .get());
}