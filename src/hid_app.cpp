#include "tusb.h"
#include <OpticalEncoder.hpp>

static void process_mouse_report(hid_mouse_report_t const *report);

// tuh_hid_mount_cb is executed when a new device is mounted.
void tuh_hid_mount_cb(uint8_t dev_addr, uint8_t instance,
                      uint8_t const *desc_report, uint16_t desc_len) {

  uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);

  if (itf_protocol == HID_ITF_PROTOCOL_NONE) {
    return;
  }

  const char *protocol_str[] = {"None", "Keyboard", "Mouse"};

  // dirty initialization fix
  auto setup = false;
  while (!setup) {
    setup = tuh_hid_receive_report(dev_addr, instance);
  }
}

// tuh_hid_report_received_cb is executed when data is received from the mouse.
void tuh_hid_report_received_cb(uint8_t dev_addr, uint8_t instance,
                                uint8_t const *report, uint16_t len) {
  switch (tuh_hid_interface_protocol(dev_addr, instance)) {
  case HID_ITF_PROTOCOL_MOUSE:
    process_mouse_report((hid_mouse_report_t const *)report);
    break;
  }
  tuh_hid_receive_report(dev_addr, instance);
}

// tuh_hid_umount_cb is executed when a device is unmounted.
void tuh_hid_umount_cb(uint8_t dev_addr, uint8_t instance){};

static void process_mouse_report(hid_mouse_report_t const *report) {
  updateOpticalEncoder(report->x, report->y);
};
