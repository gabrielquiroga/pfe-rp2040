#include <SimpsonIntegrator.hpp>

SimpsonIntegrator::SimpsonIntegrator() {
  acum = 0;
  // delta = 0.0;

  fa = 0.0;
  fm = 0.0;
  fb = 0.0;

  dt_am = 0.0;
  dt_mb = 0.0;
};

SimpsonIntegrator::~SimpsonIntegrator(){};

float SimpsonIntegrator::integrate(float newValue, float newDt) {
  addValue(newValue);
  addInterval(newDt);

  if (acum == 2) {
    acum--;
    return (fa + 4 * fm + fb) * (dt_am + dt_mb) / 6;
  } else {
    acum++;
    return 0;
  }
  // return delta;
};

void SimpsonIntegrator::addValue(float newValue) {
  fa = fm;
  fm = fb;
  fb = newValue;
}

void SimpsonIntegrator::addInterval(float newDt) {
  dt_am = dt_mb;
  dt_mb = newDt;
}