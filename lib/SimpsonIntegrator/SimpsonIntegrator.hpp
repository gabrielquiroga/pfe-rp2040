#pragma once

#include "interfaces/IIntegrator.hpp"

class SimpsonIntegrator : public IIntegrator {
private:
  uint8_t acum;
  // float delta;
  float fa;
  float fm;
  float fb;

  float dt_am;
  float dt_mb;

public:
  SimpsonIntegrator();
  ~SimpsonIntegrator();
  float integrate(float newValue, float newDt) override;
  void addValue(float newValue);
  void addInterval(float newDt);
};