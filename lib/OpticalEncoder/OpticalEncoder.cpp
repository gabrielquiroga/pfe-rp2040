#include <OpticalEncoder.hpp>

void updateOpticalEncoder(int8_t dx, int8_t dy) {
  auto &encoder = OpticalEncoder::instance();
  encoder.setDeltaX(dx);
  encoder.setDeltaY(dy);
};