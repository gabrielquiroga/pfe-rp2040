#pragma once
#include "interfaces/NonCopyable.hpp"
#include "interfaces/NonMovable.hpp"

void updateOpticalEncoder(int8_t dx, int8_t dy);

class OpticalEncoder : NonCopyable, NonMovable {
  friend void updateOpticalEncoder(int8_t dx, int8_t dy);

private:
  OpticalEncoder()
      : deltaX(0), deltaY(0), accumulatedX(0), accumulatedY(0), scale(1){};

  int8_t deltaX;
  int8_t deltaY;

  int accumulatedX;
  int accumulatedY;

  float scale;

  void setDeltaX(auto dx) {
    deltaX = dx;
    accumulatedX += dx;
  };
  void setDeltaY(auto dy) {
    deltaY = dy;
    accumulatedY += dy;
  };

public:
  static auto &instance() {
    static OpticalEncoder test;
    return test;
  }

  auto getDeltaX() const { return deltaX; };
  auto getDeltaY() const { return deltaY; };

  auto getAccumulatedX() { return accumulatedX; };
  auto getAccumulatedY() { return accumulatedY; };

  void setScale(auto pixels2meters) { scale = pixels2meters; };

  auto getScaledDeltaX() const { return deltaX; };
  auto getScaledDeltaY() const { return deltaY; };

  auto getScaledAccumulatedX() { return accumulatedX * scale; };
  auto getScaledAccumulatedY() { return accumulatedY * scale; };

  void reset() {
    setDeltaX(0);
    setDeltaY(0);
    accumulatedX = 0;
    accumulatedY = 0;
  }
};
