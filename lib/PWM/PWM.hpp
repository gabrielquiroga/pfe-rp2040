#pragma once
#include "interfaces/IBuilder.hpp"
#include "interfaces/IPWM.hpp"
#include <Pin.hpp>
#include <memory>

class PWM : public IPWM {
private:
  std::unique_ptr<IPin> pin;
  const uint clock = 125000000;
  uint slice;
  uint channel;
  uint divider;
  uint wrap;
  uint top;
  uint level;

public:
  typedef struct {
    std::unique_ptr<IPin> pin;
    uint frequency = 500;
    uint duty = 0;
  } Config;
  class Builder;
  explicit PWM(std::unique_ptr<IPin> pin, const uint frequency = 500,
               const uint duty = 0);
  PWM(Config config);
  ~PWM();
  uint frequency() const override;
  void frequency(const uint frequency) override;
  uint duty() const override;
  void duty(const uint duty) override;
  void deinit() const override;
};

class PWM::Builder : IBuilder<PWM> {
private:
  PWM::Config config;

public:
  Builder &at(uint pin) {
    this->config.pin = std::move(
        Pin::Builder().at(pin).withFunction(IPin::Function::PWM).get());
    return *this;
  };
  Builder &at(std::unique_ptr<IPin> pin) {
    this->config.pin = std::move(pin);
    return *this;
  };
  Builder &withFrequency(uint frequency) {
    this->config.frequency = frequency;
    return *this;
  };
  Builder &withDuty(uint duty) {
    this->config.duty = duty;
    return *this;
  };

  PWM build() { return PWM{std::move(config)}; };

  std::unique_ptr<PWM> get() {
    return std::move(std::make_unique<PWM>(std::move(config)));
  };
};
