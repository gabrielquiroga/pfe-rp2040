#include <GyroProcessor.hpp>

GyroProcessor::GyroProcessor(std::unique_ptr<IFilter> firstFilter,
                             std::unique_ptr<IFilter> secondFilter,
                             std::unique_ptr<IIntegrator> integrator)
    : firstFilter(std::move(firstFilter)),
      secondFilter(std::move(secondFilter)),
      integrator(std::move(integrator)){};

GyroProcessor::GyroProcessor(Config config)
    : GyroProcessor(std::move(config.firstFilter),
                    std::move(config.secondFilter),
                    std::move(config.integrator)){};

GyroProcessor::~GyroProcessor(){};

float GyroProcessor::getAngularSpeed(float rawValue) {
  return this->secondFilter->filter(this->firstFilter->filter(rawValue));
};

float GyroProcessor::getAngle(std::pair<float, float> rawMeasure) {
  return this->integrator->integrate(getAngularSpeed(rawMeasure.first),
                                     rawMeasure.second);
};