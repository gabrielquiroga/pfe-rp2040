#include <TrapeziumIntegrator.hpp>

TrapeziumIntegrator::TrapeziumIntegrator() {
  lastValue = 0.00;
  delta = 0.00;
};

float TrapeziumIntegrator::integrate(float newValue, float dt) {

  delta = (lastValue + newValue) * dt / 2;
  lastValue = newValue;
  return delta;
};

void TrapeziumIntegrator::addValue(float newValue) { lastValue = newValue; }