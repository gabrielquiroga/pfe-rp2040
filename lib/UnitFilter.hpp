#pragma once

#include "interfaces/IFilter.hpp"

class UnitFilter : IFilter {

public:
  UnitFilter();
  ~UnitFilter();
  float filter(float rawData) override { return rawData; };
};