// #include "hardware/gpio.h"
// #include "hardware/irq.h"
#include "proxies/gpioProxy.hpp"
#include "proxies/irqProxy.hpp"
#include <IRQManager.hpp>
#include <functional>
#include <map>

// ==============================================================
// Start GPIO IRQ Handling
// ==============================================================

static std::map<uint, std::function<void(void)>> gpioISR;

void handler(uint pin, uint32_t events) {
  const auto id = (pin << 8) + events;
  if (gpioISR.size() > 0) {
    if (gpioISR.find(id) != gpioISR.end())
      if (gpioISR.at(id) != nullptr)
        gpioISR.at(id)();
  }
}

void IRQManager::registerGpioIrqHandler(
    uint pin, uint32_t events, std::function<void(void)> callback) const {
  if (gpioISR.size() == 0) {
    gpio_set_irq_enabled_with_callback(pin, events, true, handler);
  } else {
    gpio_set_irq_enabled(pin, events, true);
  }
  const auto id = (pin << 8) + events;

  gpioISR.insert({id, callback});
};
void IRQManager::unregisterGpioIrqHandler(uint pin, uint32_t events) const {
  gpio_set_irq_enabled(pin, events, false);
  if (gpioISR.size() > 0) {
    const auto id = (pin << 8) + events;
    gpioISR.erase(id);
  }
};

// ==============================================================
// End GPIO IRQ Handling
// ==============================================================

bool IRQManager::checkElementInMap(
    uint id, std::map<uint, std::function<void(void)>> map) const {
  if (map.size() > 0) {
    if (map.find(id) != map.end())
      if (map.at(id) != nullptr)
        return true;
  }
  return false;
}

// ==============================================================
// Start General IRQ Handling
// ==============================================================
typedef void (*irqHandler)(void);
static std::map<uint, std::function<void()>> irqServices;
static std::map<uint, irqHandler> irqHandlers;

template <size_t irq>
auto registerIrq = []() {
  irqHandlers.insert({irq, []() { irqServices.at(irq)(); }});
  irq_set_exclusive_handler(irq, irqHandlers.at(irq));
};

template <size_t irq>
void IRQManager::registerIrqHandler(std::function<void(void)> callback) const {

  auto canInsert = !checkElementInMap(irq, irqServices);
  if (canInsert) {
    registerIrq<irq>();
    irq_set_enabled(irq, true);
    irqServices.insert({irq, callback});
  }
};

void IRQManager::unregisterIrqHandler(uint irq) const {

  auto canErase = checkElementInMap(irq, irqServices);
  if (canErase) {
    irq_set_enabled(irq, false);
    irqServices.erase(irq);
  }
};

// ==============================================================
// End General IRQ Handling
// ==============================================================
typedef std::function<void(void)> callback_t;
// template methods instancing
template void IRQManager::registerIrqHandler<0>(callback_t callback) const;
template void IRQManager::registerIrqHandler<1>(callback_t callback) const;
template void IRQManager::registerIrqHandler<2>(callback_t callback) const;
template void IRQManager::registerIrqHandler<3>(callback_t callback) const;
template void IRQManager::registerIrqHandler<4>(callback_t callback) const;
template void IRQManager::registerIrqHandler<5>(callback_t callback) const;
template void IRQManager::registerIrqHandler<6>(callback_t callback) const;
template void IRQManager::registerIrqHandler<7>(callback_t callback) const;
template void IRQManager::registerIrqHandler<8>(callback_t callback) const;
template void IRQManager::registerIrqHandler<9>(callback_t callback) const;
template void IRQManager::registerIrqHandler<10>(callback_t callback) const;
template void IRQManager::registerIrqHandler<11>(callback_t callback) const;
template void IRQManager::registerIrqHandler<12>(callback_t callback) const;
template void IRQManager::registerIrqHandler<13>(callback_t callback) const;
template void IRQManager::registerIrqHandler<14>(callback_t callback) const;
template void IRQManager::registerIrqHandler<15>(callback_t callback) const;
template void IRQManager::registerIrqHandler<16>(callback_t callback) const;
template void IRQManager::registerIrqHandler<17>(callback_t callback) const;
template void IRQManager::registerIrqHandler<18>(callback_t callback) const;
template void IRQManager::registerIrqHandler<19>(callback_t callback) const;
template void IRQManager::registerIrqHandler<20>(callback_t callback) const;
template void IRQManager::registerIrqHandler<21>(callback_t callback) const;
template void IRQManager::registerIrqHandler<22>(callback_t callback) const;
template void IRQManager::registerIrqHandler<23>(callback_t callback) const;
template void IRQManager::registerIrqHandler<24>(callback_t callback) const;
template void IRQManager::registerIrqHandler<25>(callback_t callback) const;