#pragma once
#include "interfaces/IIRQManager.hpp"
#include <functional>
#include <map>

class IRQManager : public IIRQManager {
public:
  void
  registerGpioIrqHandler(uint pin, uint32_t events,
                         std::function<void(void)> callback) const override;
  void unregisterGpioIrqHandler(uint pin, uint32_t events) const override;

  template <size_t irq>
  void registerIrqHandler(std::function<void(void)> callback) const;

  void unregisterIrqHandler(uint irq) const override;

  bool checkElementInMap(
      uint id, std::map<uint, std::function<void(void)>> map) const override;
};
