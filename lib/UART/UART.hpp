#pragma once
#include "interfaces/Communications/IUART.hpp"
#include "interfaces/IBuilder.hpp"
#include <IRQManager.hpp>
#include <Pin.hpp>
#include <functional>
#include <memory>
#include <queue>
#include <string>

class UART : public IUART {
private:
  uint irqID;
  typedef std::unique_ptr<IPin> PIN;
  uart_inst_t *instance;
  PIN rx;
  PIN tx;
  uint baudrate = 115200;
  std::queue<std::string> messages;
  std::string buffer;
  IRQManager irqManager;
  const std::function<void(void)> defaultIrqHandler = [this]() {
    const auto incomingChar = this->read();
    this->buffer += incomingChar;
    if (incomingChar.at(0) == '\n') {
      this->messages.push(buffer);
      this->buffer = "";
    };
  };

public:
  typedef struct {
    std::unique_ptr<IPin> rx;
    std::unique_ptr<IPin> tx;
    uint baudrate = 115200;
    IUART::UARTInstance instance = IUART::UART0;
  } Config;

  class Builder;

  explicit UART(PIN rx, PIN tx, const uint baudrate = 115200,
                IUART::UARTInstance instance = UART0);
  explicit UART(Config config);
  ~UART();
  bool write(std::vector<uint8_t> data) const override;
  bool write(std::string data) const;
  bool writeline(std::string data) const override;
  std::vector<uint8_t> read(uint8_t bytes, uint8_t registry = 0) const override;
  std::string read() const override;
  std::string readline() const override;
  uint hasMessages() const override;
  std::string getMessage() override;
  void irq(std::function<void(void)> handler) const override;
  void irq() const override;
  std::function<void(void)> getDefaultHandler() const override {
    return defaultIrqHandler;
  };
};

class UART::Builder : IBuilder<UART> {
private:
  Config config;

public:
  Builder &withRXat(uint rx) {
    this->config.rx =
        std::make_unique<Pin>(rx, IPin::Direction::NO_DIRECTION,
                              IPin::Pull::NO_PULL, IPin::Function::UART);
    return *this;
  }
  Builder &withRX(std::unique_ptr<IPin> rx) {
    this->config.rx = std::move(rx);
    return *this;
  }
  Builder &withTXat(uint tx) {
    this->config.tx =
        std::make_unique<Pin>(tx, IPin::Direction::NO_DIRECTION,
                              IPin::Pull::NO_PULL, IPin::Function::UART);
    return *this;
  }
  Builder &withTX(std::unique_ptr<IPin> tx) {
    this->config.tx = std::move(tx);
    return *this;
  }
  Builder &withBaudrate(uint baudrate) {
    this->config.baudrate = baudrate;
    return *this;
  }
  Builder &withInstance(IUART::UARTInstance instance) {
    this->config.instance = instance;
    return *this;
  }

  UART build() { return UART(std::move(this->config)); };

  std::unique_ptr<UART> get() {
    return std::move(std::make_unique<UART>(std::move(this->config)));
  };
};
