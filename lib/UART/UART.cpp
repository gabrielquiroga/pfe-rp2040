#include "proxies/uartProxy.hpp"
#include <UART.hpp>

UART::UART(PIN rx, PIN tx, const uint baudrate, IUART::UARTInstance instance)
    : rx(std::move(rx)), tx(std::move(tx)), baudrate(baudrate) {
  this->instance = instance == UART0 ? uart0 : uart1;
  uart_init(this->instance, this->baudrate);
  this->irqID =
      instance == UART0 ? IRQManager::uart0_irq : IRQManager::uart1_irq;
};

UART::UART(Config config)
    : UART(std::move(config.rx), std::move(config.tx), config.baudrate,
           config.instance){};

UART::~UART() { uart_deinit(this->instance); };

bool UART::write(std::vector<uint8_t> data) const {
  const std::string payload(data.begin(), data.end());
  // uart_puts(this->instance, payload.c_str());
  uart_write_blocking(this->instance, data.data(), data.size());
  return true;
};

bool UART::write(std::string data) const {
  uart_puts(this->instance, data.c_str());
  return true;
};
bool UART::writeline(std::string data) const {
  const auto payload = data + '\n';
  return write(payload);
};

std::vector<uint8_t> UART::read(uint8_t bytes, uint8_t registry) const {
  auto data = std::vector<uint8_t>(bytes);
  uart_read_blocking(this->instance, data.data(), bytes);
  return data;
};

std::string UART::read() const {
  auto buffer = std::string();
  while (uart_is_readable(this->instance)) {
    buffer += uart_getc(this->instance);
  }
  return buffer;
};
std::string UART::readline() const {
  auto buffer = std::string();
  // while (uart_is_readable_within_us(this->instance, 1000)) {
  while (uart_is_readable(this->instance)) {
    auto data = uart_getc(this->instance);
    if (data == '\n')
      return buffer;
    buffer += data;
  }
  // return buffer;
  return "";
};

void UART::irq(std::function<void(void)> handler) const {

  uart_set_fifo_enabled(uart0, false);
  uart_set_irq_enables(uart0, true, false);
  if (handler == nullptr) {
    if (instance == uart0) {
      irqManager.registerIrqHandler<IRQManager::uart0_irq>(
          this->defaultIrqHandler);
    } else {
      irqManager.registerIrqHandler<IRQManager::uart1_irq>(
          this->defaultIrqHandler);
    }
  } else {
    if (instance == uart0) {
      irqManager.registerIrqHandler<IRQManager::uart0_irq>(handler);
    } else {
      irqManager.registerIrqHandler<IRQManager::uart1_irq>(handler);
    }
  }
};

void UART::irq() const {
  uart_set_fifo_enabled(instance, true);
  uart_set_irq_enables(instance, false, false);
  const auto irqID =
      instance == uart0 ? irqManager.uart0_irq : irqManager.uart1_irq;

  irqManager.unregisterIrqHandler(irqID);
};

// bool UART::hasMessages() const { return !messages.empty(); }
uint UART::hasMessages() const { return messages.size(); }

std::string UART::getMessage() {
  if (!messages.empty()) {
    const auto message = messages.front();
    messages.pop();
    return message;
  }
  return "";
}
