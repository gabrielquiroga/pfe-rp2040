#pragma once

#include "interfaces/Communications/IMessageParser.hpp"
#include <rapidjson/document.h>

class MessageParser : public IMessageParser<std::pair<float, float>> {
private:
  rapidjson::Document document;
  float getValue(std::string key);

public:
  std::pair<float, float> parse(std::string data) override;
  std::string encode(std::pair<float, float> data) const override;
};
