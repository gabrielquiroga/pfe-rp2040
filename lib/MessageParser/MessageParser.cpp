#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include <MessageParser.hpp>

using namespace rapidjson;

float MessageParser::getValue(std::string key) {
  auto const &it = document.FindMember(key.c_str());
  return it != document.MemberEnd() ? it->value.GetFloat() : 0.0;
}

std::pair<float, float> MessageParser::parse(std::string data) {
  if (document.Parse(data.c_str()).HasParseError()) {
    return std::make_pair(0.0, 0.0);
  };

  if (document.IsObject() && !document.HasParseError()) {
    return std::make_pair(getValue("x"), getValue("o"));
  }
  return std::make_pair(0.0, 0.0);
};

std::string MessageParser::encode(std::pair<float, float> data) const {

  auto &&[_x, _o] = data;

  Document d; // Null
  d.SetObject();

  Value x(_x);
  Value o(_o);

  d.AddMember("x", x, d.GetAllocator());
  d.AddMember("o", o, d.GetAllocator());

  StringBuffer buffer;
  Writer<StringBuffer> writer(buffer);
  writer.SetMaxDecimalPlaces(3);
  d.Accept(writer);

  return std::string(buffer.GetString());
};