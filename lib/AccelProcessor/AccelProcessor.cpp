#include <AccelProcessor.hpp>

AccelProcessor::AccelProcessor(std::unique_ptr<IFilter> aFil,
                               std::unique_ptr<IFilter> bFIl,
                               std::unique_ptr<IIntegrator> aInteg) {
  //  std::unique_ptr<IIntegrator> bInteg) {
  this->aFil = std::move(aFil);
  this->bFil = std::move(bFil);
  this->aInteg = std::move(aInteg);
  // this->bInteg = std::move(bInteg);
};

AccelProcessor::~AccelProcessor(){};

float AccelProcessor::getAcceleration(float rawValue) {
  return this->bFil->filter(this->aFil->filter(rawValue));
};

float AccelProcessor::getLinearSpeed(std::pair<float, float> rawMeasure) {
  return this->aInteg->integrate(getAcceleration(rawMeasure.first),
                                 rawMeasure.second);
};

// float AccelProcessor::getPosition(std::pair<float, float> rawMeasure) {
//   return this->bInteg->integrate(getLinearSpeed(rawMeasure),
//   rawMeasure.second);
// };