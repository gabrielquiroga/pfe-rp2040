#include "proxies/i2cProxy.hpp"
#include <I2C.hpp>
#include <array>
#include <vector>
#ifndef NATIVE
#include "pico/binary_info.h"
#endif

I2C::I2C(std::unique_ptr<IPin> SDA, std::unique_ptr<IPin> SCL,
         const uint frequency, II2C::I2CInstance instance)
    : SDA(std::move(SDA)), SCL(std::move(SCL)) {
  this->instance = instance == I2C0 ? i2c0 : i2c1;
  i2c_init(this->instance, frequency);

#ifndef NATIVE
  bi_decl(bi_2pins_with_func(this->SDA->getPin(), this->SCL->getPin(),
                             IPin::Function::I2C));
#endif
};

I2C::I2C(Config config)
    : I2C(std::move(config.SDA), std::move(config.SCL), config.frequency,
          config.instance){};

I2C ::~I2C() { deinit(); };
std::vector<uint8_t> I2C::scan() const {
  auto addresses = std::vector<uint8_t>();
  for (int address = 0; address < (1 << 7); ++address) {
    uint response;
    uint8_t rxData;
    if (isReservedAddress(address))
      response = PICO_ERROR_GENERIC;
    else
      response = i2c_read_blocking(this->instance, address, &rxData, 1, false);

    if (response == 1)
      addresses.push_back(address);
  }
  return addresses;
};
bool I2C::write(std::vector<uint8_t> data) const {
  if (address == 0x00)
    return false;

  return writeTo(data, address);
};
bool I2C::writeTo(std::vector<uint8_t> data, uint8_t address) const {

  const uint8_t success = i2c_write_blocking(this->instance, address,
                                             data.data(), data.size(), false);
  return success == data.size();
};
std::vector<uint8_t> I2C::read(uint8_t bytes, uint8_t registry) const {
  if (address == 0x00)
    return std::vector<uint8_t>(1, 0);
  return readFrom(bytes, registry, this->address);
};
std::vector<uint8_t> I2C::readFrom(uint8_t bytes, uint8_t registry,
                                   uint8_t address) const {
  auto data = std::vector<uint8_t>(bytes);
  auto remoteRegistry = std::array<uint8_t, 1>({registry});

  i2c_write_blocking(this->instance, address, remoteRegistry.data(), 1, true);
  i2c_read_blocking(this->instance, address, data.data(), bytes, false);
  return data;
};
void I2C::deinit() const { i2c_deinit(this->instance); };
void I2C::setAddress(uint8_t address) { this->address = address; };

bool I2C::isReservedAddress(uint8_t address) const {
  return (address & 0x78) == 0 || (address & 0x78) == 0x78;
}

int I2C::readWord(uint8_t registry, uint8_t size) const {
  auto data = read(size, registry);
  if (data.size() != size)
    return 0;

  auto word = 0;
  auto i = size - 1U;
  for (const auto &byte : data) {
    word |= byte << (8 * i);
    --i;
  };
  switch (size) {
  case 1:
    return static_cast<int8_t>(word);
    break;
  case 2:
    return static_cast<int16_t>(word);
    break;
  case 3:
  case 4:
    return static_cast<int32_t>(word);
    break;
  default:
    return static_cast<int64_t>(word);
    break;
  }

  // return word;
}

bool I2C::writeWord(uint8_t registry, int word, uint8_t size) const {
  auto data = std::vector<uint8_t>(size + 1, 0);
  uint8_t i = size + 1;
  data.at(0) = registry;
  for (auto &item : data) {
    if (i-- == 0) {
      // item = registry;
      continue;
    }
    item = (word >> (i * 8));
  }
  return write(data);
};

bool I2C::writeWord(uint8_t registry, int16_t data) const {
  return writeWord(registry, data, 2);
};
bool I2C::writeWord(uint8_t registry, int32_t data) const {
  return writeWord(registry, data, 4);
};