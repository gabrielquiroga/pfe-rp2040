#include <LeakyFilter.hpp>

LeakyFilter::LeakyFilter(float builderLambda) {
  pastValue = 0.0;
  setLambda(builderLambda);
};
LeakyFilter::~LeakyFilter(){};

void LeakyFilter::setLambda(float newLambda) { lambda = newLambda; };

float LeakyFilter::filter(float currentMeasure) {

  auto filtered = lambda * pastValue + (1 - lambda) * currentMeasure;
  pastValue = currentMeasure;

  return filtered;
};