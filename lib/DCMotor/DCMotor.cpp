#include <DCMotor.hpp>

const auto clamp = [](int16_t value) {
  return abs(value) >= 100 ? 100 : abs(value);
};

DCMotor::DCMotor(std::unique_ptr<IPin> forwardPin,
                 std::unique_ptr<IPin> reversePin,
                 std::unique_ptr<IPWM> speedControl)
    : currentSpeed(0), currentDirection(true),
      forwardPin(std::move(forwardPin)), reversePin(std::move(reversePin)),
      speedControl(std::move(speedControl)){};

DCMotor::DCMotor(Config config)
    : DCMotor(std::move(config.forwardPin), std::move(config.reversePin),
              std::move(config.speedControl)){};

DCMotor::~DCMotor(){};

void DCMotor::move(int8_t speed) {
  if (speed > 0)
    return forward(speed);

  if (speed < 0)
    return reverse(speed);

  stop();
};
void DCMotor::forward(int8_t speed) {

  const auto clampedSpeed = clamp(speed);
  this->forwardPin->on();
  this->reversePin->off();
  this->speedControl->duty(clampedSpeed);
  this->currentSpeed = clampedSpeed;
  this->currentDirection = true;
};
void DCMotor::reverse(int8_t speed) {
  const auto clampedSpeed = clamp(speed);
  this->forwardPin->off();
  this->reversePin->on();
  this->speedControl->duty(clampedSpeed);
  this->currentSpeed = -clampedSpeed;
  this->currentDirection = false;
};
void DCMotor::stop() {
  this->forwardPin->off();
  this->reversePin->off();
  this->speedControl->duty(0);
  this->currentSpeed = 0;
};
bool DCMotor::getDirection() const { return currentDirection; };
int8_t DCMotor::getSpeed() const { return currentSpeed; };