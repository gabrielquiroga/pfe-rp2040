#pragma once

#include "interfaces/IBuilder.hpp"
#include "interfaces/IPWM.hpp"
#include "interfaces/Motors/IDCMotor.hpp"
#include <PWM.hpp>
#include <Pin.hpp>
#include <cmath>
#include <memory>

class DCMotor : public IDCMotor {
private:
  int16_t currentSpeed;
  bool currentDirection;
  std::unique_ptr<IPin> forwardPin;
  std::unique_ptr<IPin> reversePin;
  std::unique_ptr<IPWM> speedControl;

public:
  typedef struct {
    std::unique_ptr<IPin> forwardPin;
    std::unique_ptr<IPin> reversePin;
    std::unique_ptr<IPWM> speedControl;
  } Config;
  class Builder;
  explicit DCMotor(std::unique_ptr<IPin> forwardPin,
                   std::unique_ptr<IPin> reversePin,
                   std::unique_ptr<IPWM> speedControl);
  DCMotor(Config config);
  ~DCMotor();
  void move(const int8_t speed) override;
  void forward(const int8_t speed) override;
  void reverse(const int8_t speed) override;
  void stop() override;
  bool getDirection() const override;
  int8_t getSpeed() const override;
};

class DCMotor::Builder : IBuilder<DCMotor> {
private:
  DCMotor::Config config;

public:
  Builder &withForwardAt(uint pin) {
    this->config.forwardPin =
        std::move(std::make_unique<Pin>(pin, IPin::Direction::OUT));
    return *this;
  };
  Builder &withForward(std::unique_ptr<IPin> pin) {
    this->config.forwardPin = std::move(pin);
    return *this;
  };
  Builder &withReverseAt(uint pin) {
    this->config.reversePin =
        std::move(std::make_unique<Pin>(pin, IPin::Direction::OUT));
    return *this;
  };
  Builder &withReverse(std::unique_ptr<IPin> pin) {
    this->config.reversePin = std::move(pin);
    return *this;
  };
  Builder &withSpeedControlAt(uint pin) {
    this->config.speedControl = std::move(PWM::Builder().at(pin).get());
    return *this;
  };
  Builder &withSpeedControl(std::unique_ptr<IPWM> pwm) {
    this->config.speedControl = std::move(pwm);
    return *this;
  };

  DCMotor build() { return DCMotor{std::move(config)}; }
  std::unique_ptr<DCMotor> get() {
    return std::move(std::make_unique<DCMotor>(std::move(config)));
  };
};