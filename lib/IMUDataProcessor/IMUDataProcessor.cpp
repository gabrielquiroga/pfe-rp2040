#include "proxies/timestampProxy.hpp"
#include <IMUDataProcessor.hpp>

IMUDataProcessor::IMUDataProcessor(std::unique_ptr<IMPU6500> mpu) {
  this->mpu = std::move(mpu);

  // auto leakyFilter = std::make_unique<LeakyFilter>(0.2);
  // auto medianFilter = std::make_unique<MedianFilter>(5);
  // auto simpsonIntegrator = std::make_unique<SimpsonIntegrator>();

  this->gyroProcessor = std::move(GyroProcessor::Builder().get());

  this->mpu->calibrateGyroAxis(MPU6500::Axis::X | MPU6500::Axis::Y |
                               MPU6500::Axis::Z);
  this->mpu->calibrateAccAxis(MPU6500::Axis::X | MPU6500::Axis::Y |
                              MPU6500::Axis::Z);

  this->lastMeasure = {this->mpu->getGyroZ(), 0.0};
};

IMUDataProcessor::~IMUDataProcessor(){};

void IMUDataProcessor::update() {
  std::pair<float, absolute_time_t> currentMeasure = {this->mpu->getGyroZ(),
                                                      get_absolute_time()};
  this->updateDeltaAngle = this->gyroProcessor->getAngle(
      {currentMeasure.first,
       absolute_time_diff_us(currentMeasure.second, lastMeasure.second) /
           1000000.0});
  this->lastMeasure = currentMeasure;
  this->currentAngle += this->updateDeltaAngle;
  this->deltaAngle += this->updateDeltaAngle;
};

float IMUDataProcessor::getUpdateDeltaAngle() const {
  return this->deltaAngle;
};

float IMUDataProcessor::getDeltaAngle() {
  auto auxiliar = deltaAngle;
  deltaAngle = 0.0;
  return auxiliar;
};

float IMUDataProcessor::getCurrentAngle() const { return this->currentAngle; };

void IMUDataProcessor::reset() {
  this->currentAngle = 0.0;
  this->lastMeasure = {0.0, get_absolute_time()};
  this->deltaAngle = 0.0;
  this->updateDeltaAngle = 0.0;
};