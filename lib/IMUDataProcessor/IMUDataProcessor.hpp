#pragma once

#include <GyroProcessor.hpp>
#include <LeakyFilter.hpp>
#include <MPU6500.hpp>
#include <MedianFilter.hpp>
#include <SimpsonIntegrator.hpp>
#include <interfaces/IMU/IIMUDataProcessor.hpp>

class IMUDataProcessor : IIMUDataProcessor {
private:
  float currentAngle = 0.0;
  float deltaAngle = 0.0;
  float updateDeltaAngle = 0.0;
  std::pair<float, uint64_t> lastMeasure;
  std::unique_ptr<IMPU6500> mpu;
  std::unique_ptr<GyroProcessor> gyroProcessor;

public:
  explicit IMUDataProcessor(std::unique_ptr<IMPU6500> mpu);
  ~IMUDataProcessor();
  void update() override;
  float getCurrentAngle() const override;
  float getDeltaAngle() override;
  float getUpdateDeltaAngle() const override;
  void reset() override;
  // float getCurrentAngularSpeed() override;
  // float getDeltaAngularSpeed() override;
};