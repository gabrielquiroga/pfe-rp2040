#pragma once
#include "config.hpp"
#include "interfaces/NonCopyable.hpp"
#include "interfaces/NonMovable.hpp"
#include <DifferentialRobot.hpp>
#include <UART.hpp>
#include <cmath>
#include <memory>
#include <utility>

class SimpleStateMachine {
  enum States { IDLE, ROTATING, TRANSLATING, CALIBRATING };

private:
  float traslationTolerance = 0.01f;
  float rotationTolerance = 0.01f;
  States currenState;
  bool firstEntry;
  void switchTo(States newState);

public:
  States getState() { return currenState; };
  SimpleStateMachine();
  SimpleStateMachine(float rotErr, float transErr);
  ~SimpleStateMachine();
  void reset();
  bool update(const std::pair<float, float> diffs, bool newTargets = false);
};

class Store : NonCopyable, NonMovable {

private:
  Store() : position(0), targetPosition(0), rotation(0), targetRotation(0){};

  float position;
  float targetPosition;
  float rotation;
  float targetRotation;
  // State state
  // Shared Objects
  std::unique_ptr<IUART> uart = nullptr;
  std::unique_ptr<IDifferentialRobot> robot = nullptr;

  SimpleStateMachine ssm{config::traslationTolerance,
                         config::rotationTolerance};

public:
  uint getState() { return this->ssm.getState(); };
  static auto &instance() {
    static Store instance;
    return instance;
  }

  auto getCurrentPosition() const { return position; };
  auto getTargetPosition() const { return targetPosition; };
  auto getCurrentRotation() const { return rotation; };
  auto getTargetRotation() const { return targetRotation; };
  auto getPositionDiff() const { return targetPosition - position; };
  auto getRotationDiff() const { return targetRotation - rotation; };
  auto getDiffs() const {
    return std::make_pair(getPositionDiff(), getRotationDiff());
  };
  void resetPosition() { position = 0; };
  void resetRotation() { rotation = 0; };
  void resetRotPos() {
    rotation = 0;
    position = 0;
  };
  void updateTargets(std::pair<float, float> targets) {
    this->ssm.reset();
    resetRotPos();
    const auto &[targetPos, targetRot] = targets;
    targetPosition = targetPos;
    targetRotation = targetRot;
    this->ssm.update(getDiffs(), true);
  };
  void updateCurrentRotPos(std::pair<float, float> data) {
    const auto &[pos, rot] = data;
    position += pos;
    rotation += rot;

    if (this->ssm.update(getDiffs())) {
      if (this->robot != nullptr)
        this->robot->stop();
    }
  };

  // Shared Objects
  void setUart(std::unique_ptr<IUART> uart) { this->uart = std::move(uart); };
  void setRobot(std::unique_ptr<IDifferentialRobot> robot) {
    this->robot = std::move(robot);
  };
  IUART &getUart() { return *this->uart; };
  IDifferentialRobot &getRobot() { return *this->robot; };
};
