#include "main/tasks.hpp"
#include <Store.hpp>

SimpleStateMachine::SimpleStateMachine()
    : currenState(IDLE), firstEntry(false){};
SimpleStateMachine::SimpleStateMachine(float trasErr, float rotErr)
    : SimpleStateMachine() {
  traslationTolerance = trasErr;
  rotationTolerance = rotErr;
};
SimpleStateMachine ::~SimpleStateMachine(){};

void SimpleStateMachine::switchTo(States newState) {
  currenState = newState;
  firstEntry = true;
}
void SimpleStateMachine::reset() { this->switchTo(IDLE); };

bool SimpleStateMachine::update(const std::pair<float, float> diffs,
                                bool newTargets) {
  const auto &[traslationErr, rotationErr] = diffs;
  switch (currenState) {
  case IDLE:
    if (newTargets)
      switchTo(ROTATING);
    break;

  case ROTATING:
    if (std::abs(rotationErr) < rotationTolerance) {
      switchTo(TRANSLATING);
      return true;
    }
    // Signal Rotation Task
    notifyTask(TaskHandleID::ROTATION_CONTROL);
    break;

  case TRANSLATING:
    if (std::abs(traslationErr) < traslationTolerance) {
      switchTo(CALIBRATING);
      return true;
    }
    // Signal Traslation Task
    notifyTask(TaskHandleID::TRASLATION_CONTROL);
    break;

  case CALIBRATING:
    // Signal Calibration Task
    if (firstEntry) {
      notifyTask(TaskHandleID::CALIBRATE_SENSORS);
    }
    switchTo(IDLE);
    break;

  default:
    break;
  };
  firstEntry = false;
  return false;
};
