#include <DifferentialRobot.hpp>

DifferentialRobot::DifferentialRobot(std::unique_ptr<IMotor> leftEngine,
                                     std::unique_ptr<IMotor> rightEngine) {
  this->leftEngine = std::move(leftEngine);
  this->rightEngine = std::move(rightEngine);
  stop();
};

DifferentialRobot::DifferentialRobot(Config config)
    : DifferentialRobot(std::move(config.leftEngine),
                        std::move(config.rightEngine)){};

DifferentialRobot::~DifferentialRobot() { stop(); };
void DifferentialRobot::move(const int8_t speed) const {
  this->leftEngine->move(speed);
  this->rightEngine->move(speed);
};
void DifferentialRobot::rotate(const int8_t speed) const {
  this->leftEngine->move(speed);
  this->rightEngine->move(-speed);
};
void DifferentialRobot::stop() const {
  this->leftEngine->stop();
  this->rightEngine->stop();
};