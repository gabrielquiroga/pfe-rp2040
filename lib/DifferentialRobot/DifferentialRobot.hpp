#pragma once
#include "interfaces/IBuilder.hpp"
#include "interfaces/Motors/IMotor.hpp"
#include "interfaces/Robot/IDifferentialRobot.hpp"
#include <memory>

class DifferentialRobot : public IDifferentialRobot {
private:
  std::unique_ptr<IMotor> leftEngine;
  std::unique_ptr<IMotor> rightEngine;

public:
  typedef struct {
    std::unique_ptr<IMotor> leftEngine;
    std::unique_ptr<IMotor> rightEngine;
  } Config;
  class Builder;

  explicit DifferentialRobot(std::unique_ptr<IMotor> leftEngine,
                             std::unique_ptr<IMotor> rightEngine);
  explicit DifferentialRobot(Config config);
  ~DifferentialRobot();

  void move(const int8_t speed) const override;
  void rotate(const int8_t speed) const override;
  void stop() const override;
};

class DifferentialRobot::Builder : IBuilder<DifferentialRobot> {

private:
  DifferentialRobot::Config config;

public:
  Builder &withLeftEngine(std::unique_ptr<IMotor> left) {
    this->config.leftEngine = std::move(left);
    return *this;
  };
  Builder &withRightEngine(std::unique_ptr<IMotor> right) {
    this->config.rightEngine = std::move(right);
    return *this;
  };

  DifferentialRobot build() { return DifferentialRobot{std::move(config)}; }
  std::unique_ptr<DifferentialRobot> get() {
    return std::move(std::make_unique<DifferentialRobot>(std::move(config)));
  };
};