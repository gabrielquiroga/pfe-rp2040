#pragma once
#include "interfaces/IBuilder.hpp"
#include "interfaces/IPin.hpp"
#include <IRQManager.hpp>
#include <memory>

class Pin : public IPin {
private:
  uint pin;
  mutable uint irqTriggers = 0;
  IRQManager irqManager;

public:
  typedef struct {
    uint pin;
    Direction direction = Direction::OUT;
    Pull pull = Pull::PULL_DOWN;
    Function function = Function::NONE;
  } Config;

  class Builder;

  explicit Pin(const int pin, Direction direction = Direction::OUT,
               Pull pull = Pull::PULL_DOWN, Function function = Function::NONE);
  explicit Pin(Config config);
  ~Pin();
  uint getPin() const override;
  bool value() const override;
  void value(bool value) const override;
  void toggle() const override;
  void on() const override;
  void off() const override;
  void pull(Pull mode) const override;
  void irq(uint triggers, std::function<void(void)> handler) const override;
  void irq() const override;
};

class Pin::Builder : IBuilder<Pin> {
private:
  Pin::Config config;

public:
  Builder &at(uint pin) {
    this->config.pin = pin;
    return *this;
  }
  Builder &withDirection(IPin::Direction direction) {
    this->config.direction = direction;
    return *this;
  }
  Builder &withPull(IPin::Pull pull) {
    this->config.pull = pull;
    return *this;
  }
  Builder &withFunction(IPin::Function function) {
    this->config.function = function;
    return *this;
  }

  Pin build() { return Pin{std::move(config)}; };

  std::unique_ptr<Pin> get() {
    return std::move(std::make_unique<Pin>(std::move(config)));
  };
};