#include "proxies/gpioProxy.hpp"
#include <Pin.hpp>

Pin::Pin(const int pin, Direction direction, Pull pull, Function function)
    : pin(pin), irqManager(std::move(irqManager)) {
  gpio_init(pin);

  if (direction != Direction::NO_DIRECTION) {
    gpio_set_dir(pin, static_cast<bool>(direction));
    if (pull != Pull::NO_PULL) {
      if (direction == Direction::IN)
        pull == Pull::PULL_UP ? gpio_pull_up(pin) : gpio_pull_down(pin);
      else
        gpio_put(pin, 0);
    }
  }

  if (function != Function::NONE)
    gpio_set_function(pin, static_cast<gpio_function>(function));
}

Pin::Pin(Config config)
    : Pin(config.pin, config.direction, config.pull, config.function){};

Pin::~Pin() { irq(); };

uint Pin::getPin() const { return pin; }
bool Pin::value() const { return gpio_get(pin); };
void Pin::value(bool value) const { gpio_put(pin, value); };
void Pin::toggle() const { gpio_put(pin, !gpio_get(pin)); };
void Pin::on() const { gpio_put(pin, true); };
void Pin::off() const { gpio_put(pin, false); };
void Pin::pull(Pull mode) const {
  mode == Pull::PULL_DOWN ? gpio_pull_down(pin) : gpio_pull_up(pin);
};
void Pin::irq(uint triggers, std::function<void(void)> handler) const {
  if (handler == nullptr) {
    gpio_set_irq_enabled(pin, irqTriggers, false);
    irqTriggers = 0;
    return;
  }
  irqManager.registerGpioIrqHandler(this->pin, triggers, handler);
  irqTriggers = triggers;
};
void Pin::irq() const {
  if (irqTriggers == 0)
    return;

  irqManager.unregisterGpioIrqHandler(this->pin, this->irqTriggers);
  irqTriggers = 0;
};
