#include <PID.hpp>

#include "proxies/timestampProxy.hpp"
#include <bits/stdc++.h>

PID::PID(float kp, float ki, float kd) : Kp(kp), Ki(ki), Kd(kd){};

PID::~PID() {}

int8_t PID::operator()(float error) {
  auto currentTime = get_absolute_time();
  auto dt = absolute_time_diff_us(timestamp, currentTime) / 10e6;
  auto p = error;
  auto i = (error + previousError) * dt / 2;
  auto d = (error) / dt;

  previousError = error;
  timestamp = currentTime;

  auto action = p * Kp + i * Ki + d * Kd;

  return static_cast<int8_t>(std::clamp(action, -100.0, 100.0));
};