#pragma once

class PID {
private:
  float Kp;
  float Ki;
  float Kd;
  uint64_t timestamp;
  float previousError;
  float previousProportionalAction;

public:
  explicit PID(float kp, float ki, float kd);
  ~PID();
  int8_t operator()(float error);
};
