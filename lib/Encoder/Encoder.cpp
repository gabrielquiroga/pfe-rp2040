#include "Encoder.hpp"
#include <map>

Encoder::Encoder(std::unique_ptr<IPin> input, float step, uint event,
                 std::unique_ptr<IPin> directionPin, bool direction,
                 std::function<void(void)> fn)
    : input(std::move(input)), step(step), event(event),
      directionPin(std::move(directionPin)), direction(direction),
      callback(fn) {

  this->input->irq(this->event, [this]() { this->update(); });
};

Encoder::Encoder(Config config)
    : Encoder(std::move(config.input), config.step, config.event,
              std::move(config.directionPin), true, config.callback){};

Encoder ::~Encoder(){};

void Encoder::update() {
  if (this->directionPin != nullptr)
    steps += this->directionPin->value() ? 1 : -1;
  else
    steps += this->direction ? 1 : -1;

  if (this->callback != nullptr)
    this->callback();
}