#pragma once

#include "interfaces/Communications/II2C.hpp"
#include "interfaces/IMU/IMPU6500.hpp"
#include <interfaces/IBuilder.hpp>
#include <memory>

class MPU6500 : public IMPU6500 {
private:
  std::unique_ptr<II2C> i2c;
  uint8_t devAddress;

  void init() const;

  float getAcceleration(uint8_t address) const;
  float getGyro(uint8_t address) const;
  void setGyroXOffset(float newOffset);
  void setGyroYOffset(float newOffset);
  void setGyroZOffset(float newOffset);
  void setAccXOffset(float newOffset);
  void setAccYOffset(float newOffset);
  void setAccZOffset(float newOffset);

  // void setGyroXThreshold(float newThreshold);
  // void setGyroYThreshold(float newThreshold);
  // void setGyroZThreshold(float newThreshold);
  // void setAccXThreshold(float newThreshold);
  // void setAccYThreshold(float newThreshold);
  // void setAccZThreshold(float newThreshold);

  void calibrateXGyro();
  void calibrateYGyro();
  void calibrateZGyro();
  void calibrateXAcc();
  void calibrateYAcc();
  void calibrateZAcc();

  uint8_t iLimit = 100;

public:
  typedef struct {
    AccelScale accelScale = AccelScale::A2;
    GyroScale gyroScale = GyroScale::G250;
    std::unique_ptr<II2C> i2c = nullptr;
  } Config;

  class Builder;

  Config MPUconfig;

  explicit MPU6500(std::unique_ptr<II2C> i2c, GyroScale gyroScale = GyroScale::G250,
          AccelScale accScale = AccelScale::A2);
  explicit MPU6500(Config config);
  ~MPU6500();

  bool isConnected() const override;

  float getGyroX() const override;
  float getGyroY() const override;
  float getGyroZ() const override;
  float getAccelerationX() const override;
  float getAccelerationY() const override;
  float getAccelerationZ() const override;

  enum Axis { X = 1, Y = 2, Z = 4 };

  float gyroXOffset = 0.0;
  float gyroYOffset = 0.0;
  float gyroZOffset = 0.0;
  float accXOffset = 0.0;
  float accYOffset = 0.0;
  float accZOffset = 0.0;

  float gyroXThreshold = 0.0;
  float gyroYThreshold = 0.0;
  float gyroZThreshold = 0.0;
  float accXThreshold = 0.0;
  float accYThreshold = 0.0;
  float accZThreshold = 0.0;

  void calibrateGyroAxis(uint8_t axis) override;
  void calibrateAccAxis(uint8_t axis) override;
};

class MPU6500::Builder : IBuilder<MPU6500> {
private:
  MPU6500::Config config;

public:
  Builder &withAccelScale(AccelScale accelScale) {
    this->config.accelScale = accelScale;
    return *this;
  };
  Builder &withGyroScale(GyroScale gyroScale) {
    this->config.gyroScale = gyroScale;
    return *this;
  };
  Builder &withI2C(std::unique_ptr<II2C> i2c) {
    this->config.i2c = std::move(i2c);
    return *this;
  };

  MPU6500 build() { return MPU6500{std::move(this->config)}; };

  std::unique_ptr<MPU6500> get() {
    return std::move(std::make_unique<MPU6500>(std::move(this->config)));
  };
};

// namespace IMU::MPU6500 {

// } // namespace IMU::MPU6500
