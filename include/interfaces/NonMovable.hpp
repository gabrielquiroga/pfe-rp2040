#pragma once

class NonMovable {
public:
  NonMovable(NonMovable &&) = delete;
  NonMovable &operator=(NonMovable &&) = delete;
  NonMovable(){};
};