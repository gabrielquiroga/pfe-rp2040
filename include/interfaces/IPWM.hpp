#pragma once

#include "interfaces/NonCopyable.hpp"

class IPWM : NonCopyable {
public:
  virtual ~IPWM() {}
  virtual uint frequency() const = 0;
  virtual void frequency(const uint frequency) = 0;
  virtual uint duty() const = 0;
  virtual void duty(const uint duty) = 0;
  virtual void deinit() const = 0;
};