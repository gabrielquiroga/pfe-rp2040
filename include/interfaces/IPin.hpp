#pragma once
#include "interfaces/NonCopyable.hpp"
#include <functional>

class IPin : NonCopyable {
public:
  enum Direction { IN = false, OUT = true, NO_DIRECTION };
  enum Pull { PULL_UP = false, PULL_DOWN = true, NO_PULL };
  enum IRQTrigger { LOW = 0x1u, HIGH = 0x2u, FALLING = 0x4u, RISING = 0x8u };
  enum Function {
    XIP = 0,
    SPI = 1,
    UART = 2,
    I2C = 3,
    PWM = 4,
    SIO = 5,
    PIO0 = 6,
    PIO1 = 7,
    GPCK = 8,
    USB = 9,
    NONE = 0x1f
  };
  virtual ~IPin() {}
  virtual uint getPin() const = 0;
  virtual bool value() const = 0;
  virtual void value(bool value) const = 0;
  virtual void toggle() const = 0;
  virtual void on() const = 0;
  virtual void off() const = 0;
  virtual void pull(Pull mode) const = 0;
  virtual void irq(uint triggers, std::function<void(void)> handler) const = 0;
  virtual void irq() const = 0;
};