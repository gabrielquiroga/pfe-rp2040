#pragma once

#include "interfaces/NonCopyable.hpp"

class IMotor : NonCopyable {
public:
  virtual ~IMotor(){};
  virtual void move(const int8_t speed) = 0;
  virtual void forward(const int8_t speed) = 0;
  virtual void reverse(const int8_t speed) = 0;
  virtual void stop() = 0;
  virtual bool getDirection() const = 0;
  virtual int8_t getSpeed() const = 0;
};