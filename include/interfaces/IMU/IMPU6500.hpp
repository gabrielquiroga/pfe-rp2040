#pragma once

#include "interfaces/IMU/IMotionSensor.hpp"

class IMPU6500 : public IMotionSensor {
public:
  enum AccelScale { A2, A4, A8, A16 };
  enum GyroScale { G250, G500, G1000, G2000 };
  enum Axis { X = 1, Y = 2, Z = 4 };
  virtual ~IMPU6500(){};
  virtual bool isConnected() const = 0;
  virtual void calibrateGyroAxis(uint8_t axis) = 0;
  virtual void calibrateAccAxis(uint8_t axis) = 0;
};