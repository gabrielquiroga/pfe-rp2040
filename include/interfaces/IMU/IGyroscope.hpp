#pragma once

#include "interfaces/NonCopyable.hpp"

class IGyroscope : NonCopyable {

public:
  virtual ~IGyroscope(){};
  virtual float getGyroX() const = 0;
  virtual float getGyroY() const = 0;
  virtual float getGyroZ() const = 0;
};