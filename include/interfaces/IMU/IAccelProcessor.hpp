#pragma once

#include "interfaces/NonCopyable.hpp"
#include <utility>

class IAccelProcessor : NonCopyable {
  // private:
  //   std::pair<float, float> measure;

public:
  virtual ~IAccelProcessor(){};
  virtual float getAcceleration(float rawValue) = 0;
  virtual float getLinearSpeed(std::pair<float, float> rawMeasure) = 0;
  // virtual float getPosition(std::pair<float, float> rawMeasure) = 0;
};
