#pragma once

#include "interfaces/NonCopyable.hpp"

class IAccelerometer : NonCopyable {

public:
  virtual ~IAccelerometer(){};
  virtual float getAccelerationX() const = 0;
  virtual float getAccelerationY() const = 0;
  virtual float getAccelerationZ() const = 0;
};