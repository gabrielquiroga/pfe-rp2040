#pragma once

#include "IAccelerometer.hpp"
#include "IGyroscope.hpp"

class IMotionSensor : public IAccelerometer, public IGyroscope {
public:
  virtual ~IMotionSensor(){};
};