#pragma once
#include "interfaces/NonCopyable.hpp"
#include <vector>

template <typename T> class ICommunications : NonCopyable {

public:
  virtual ~ICommunications(){};
  virtual bool write(T data) const = 0;
  virtual std::vector<uint8_t> read(uint8_t bytes,
                                    uint8_t address = 0) const = 0;
};
