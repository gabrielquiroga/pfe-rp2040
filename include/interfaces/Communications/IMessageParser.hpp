#pragma once

#include "interfaces/NonCopyable.hpp"
#include <string>
template <typename T> class IMessageParser : NonCopyable {
public:
  virtual T parse(std::string data) = 0;
  virtual std::string encode(T) const = 0;
};