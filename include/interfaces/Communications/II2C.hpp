#pragma once

#include "interfaces/Communications/ICommunications.hpp"
#include "interfaces/NonCopyable.hpp"
#include <vector>

class II2C : ICommunications<std::vector<uint8_t>> {
public:
  enum I2CInstance { I2C0, I2C1 };
  virtual ~II2C(){};
  virtual std::vector<uint8_t> scan() const = 0;
  virtual bool write(std::vector<uint8_t> data) const override = 0;
  virtual std::vector<uint8_t> read(uint8_t bytes,
                                    uint8_t registry) const override = 0;
  virtual bool writeTo(std::vector<uint8_t> data, uint8_t address) const = 0;
  virtual std::vector<uint8_t> readFrom(uint8_t bytes, uint8_t registry,
                                        uint8_t address) const = 0;
  virtual void deinit() const = 0;
  virtual void setAddress(uint8_t address) = 0;
  virtual int readWord(uint8_t registry, uint8_t size) const = 0;

  virtual bool writeWord(uint8_t registry, int16_t data) const = 0;
  virtual bool writeWord(uint8_t registry, int32_t data) const = 0;
};
