#pragma once

#include "interfaces/Communications/ICommunications.hpp"
#include <functional>
#include <string>
#include <vector>

class IUART : ICommunications<std::vector<uint8_t>> {
public:
  enum UARTInstance { UART0, UART1 };
  virtual ~IUART(){};
  virtual bool write(std::vector<uint8_t> data) const override = 0;
  virtual bool writeline(std::string data) const = 0;
  virtual std::vector<uint8_t> read(uint8_t bytes,
                                    uint8_t registry = 0) const override = 0;
  virtual std::string read() const = 0;
  virtual std::string readline() const = 0;
  virtual uint hasMessages() const = 0;
  virtual std::string getMessage() = 0;
  virtual void irq(std::function<void(void)> handler) const = 0;
  virtual void irq() const = 0;
  virtual std::function<void(void)> getDefaultHandler() const = 0;
};
