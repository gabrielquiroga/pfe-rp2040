#pragma once
#include <memory>
template <typename T> class IBuilder {
public:
  virtual T build() = 0;
  virtual std::unique_ptr<T> get() = 0;
};