#pragma once

#include "interfaces/NonCopyable.hpp"

class IIntegrator : NonCopyable {
public:
  virtual ~IIntegrator(){};
  virtual float integrate(float rawData, float dt) = 0;
};