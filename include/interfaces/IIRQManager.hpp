#pragma once
#include <functional>
#include <map>

class IIRQManager {

public:
  virtual void
  registerGpioIrqHandler(uint pin, uint32_t events,
                         std::function<void(void)> callback) const = 0;

  virtual void unregisterGpioIrqHandler(uint pin, uint32_t events) const = 0;

  void registerIrqHandler(std::function<void(void)> callback) const {};

  virtual void unregisterIrqHandler(uint irq) const = 0;

  virtual bool
  checkElementInMap(uint id,
                    std::map<uint, std::function<void(void)>> map) const = 0;

  static const uint timer_irq_0 = 0;
  static const uint timer_irq_1 = 1;
  static const uint timer_irq_2 = 2;
  static const uint timer_irq_3 = 3;
  static const uint pwm_irq_wrap = 4;
  static const uint usbctrl_irq = 5;
  static const uint xip_irq = 6;
  static const uint pio0_irq_0 = 7;
  static const uint pio0_irq_1 = 8;
  static const uint pio1_irq_0 = 9;
  static const uint pio1_irq_1 = 10;
  static const uint dma_irq_0 = 11;
  static const uint dma_irq_1 = 12;
  static const uint io_irq_bank0 = 13;
  static const uint io_irq_qspi = 14;
  static const uint sio_irq_proc0 = 15;
  static const uint sio_irq_proc1 = 16;
  static const uint clocks_irq = 17;
  static const uint spi0_irq = 18;
  static const uint spi1_irq = 19;
  static const uint uart0_irq = 20;
  static const uint uart1_irq = 21;
  static const uint adc0_irq_fifo = 22;
  static const uint i2c0_irq = 23;
  static const uint i2c1_irq = 24;
  static const uint rtc_irq = 25;
};

// template methods instancing
// template void
// IIRQManager::registerIrqHandler<0>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<1>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<2>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<3>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<4>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<5>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<6>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<7>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<8>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<9>(std::function<void(void)> callback) const;
// template void
// IIRQManager::registerIrqHandler<10>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<11>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<12>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<13>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<14>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<15>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<16>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<17>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<18>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<19>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<20>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<21>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<22>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<23>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<24>(std::function<void(void)> callback)
// const; template void
// IIRQManager::registerIrqHandler<25>(std::function<void(void)> callback)
// const;