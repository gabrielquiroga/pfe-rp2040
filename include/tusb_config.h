#ifndef _PICO_STDIO_USB_TUSB_CONFIG_H
#define _PICO_STDIO_USB_TUSB_CONFIG_H

#if 0 /* usb device for printf() */
#include "pico/stdio_usb.h"

#define CFG_TUSB_RHPORT0_MODE (OPT_MODE_DEVICE)
#define CFG_TUD_CDC (1)
#define CFG_TUD_CDC_RX_BUFSIZE (256)
#define CFG_TUD_CDC_TX_BUFSIZE (256)
#define CFG_TUD_VENDOR (0)

#else /* usb host */

#define RP2040_USB_HOST_MODE
#define CFG_TUSB_RHPORT0_MODE OPT_MODE_HOST

#define CFG_TUH_HUB 0
#define CFG_TUH_HID 1
// #define CFG_TUH_HUB 1
// #define CFG_TUH_HID 4
#define CFG_TUSB_HOST_HID_GENERIC 0
#define CFG_TUH_HID_KEYBOARD 0
#define CFG_TUH_HID_MOUSE 1
#define CFG_TUH_CDC 0
#define CFG_TUH_MSC 0
#define CFG_TUH_VENDOR 0
#define CFG_TUSB_HOST_DEVICE_MAX (CFG_TUH_HUB ? 5 : 1) // normal hub has 4 ports

#endif

#endif
