#pragma once

#include "interfaces/Motors/Sensors/IEncoder.hpp"
#include <gmock/gmock.h>

class EncoderMock : public IEncoder {
public:
  MOCK_METHOD(void, setSteps, (int steps), (override));
  MOCK_METHOD(uint, getSteps, (), (override));
  MOCK_METHOD(int, getDeltaSteps, (), (override));
  MOCK_METHOD(float, getAngle, (), (override));
  MOCK_METHOD(float, getDeltaAngle, (), (override));
  MOCK_METHOD(void, update, (), (override));
  MOCK_METHOD(void, setDirection, (bool direction), (override));
  MOCK_METHOD(bool, getDirection, (bool direction), (override));

  //   virtual float getSpeed() = 0;
  //   virtual float getDeltaSpeed() = 0;
  //   virtual float getAcceleration() = 0;
  //   virtual float getDeltaAcceleration() = 0;
};