#pragma once
#include "interfaces/Motors/IDCMotor.hpp"
#include <gmock/gmock.h>

class DCMotorMock : IDCMotor {
public:
  MOCK_METHOD(void, move, (const int8_t speed), (override));
  MOCK_METHOD(void, forward, (const int8_t speed), (override));
  MOCK_METHOD(void, reverse, (const int8_t speed), (override));
  MOCK_METHOD(void, stop, (), (override));
  MOCK_METHOD(bool, getDirection, (), (const, override));
  MOCK_METHOD(int8_t, getSpeed, (), (const, override));
};