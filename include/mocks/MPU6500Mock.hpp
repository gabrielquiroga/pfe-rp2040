#pragma once
#include "interfaces/IMU/IMPU6500.hpp"
#include <gmock/gmock.h>

class MPU6500Mock : public IMPU6500 {
public:
  MOCK_METHOD(bool, isConnected, (), (const, override));
  MOCK_METHOD(float, getAccelerationX, (), (const, override));
  MOCK_METHOD(float, getAccelerationY, (), (const, override));
  MOCK_METHOD(float, getAccelerationZ, (), (const, override));
  MOCK_METHOD(float, getGyroX, (), (const, override));
  MOCK_METHOD(float, getGyroY, (), (const, override));
  MOCK_METHOD(float, getGyroZ, (), (const, override));
  MOCK_METHOD(void, calibrateGyroAxis, (uint8_t axis), (override));
  MOCK_METHOD(void, calibrateAccAxis, (uint8_t axis), (override));
};