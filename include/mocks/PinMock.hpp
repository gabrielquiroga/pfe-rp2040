#pragma once
#include "interfaces/IPin.hpp"
#include <gmock/gmock.h>

class PinMock : public IPin {
  typedef std::function<void(void)> IRQCallback;

public:
  MOCK_METHOD(uint, getPin, (), (const, override));
  MOCK_METHOD(bool, value, (), (const, override));
  MOCK_METHOD(void, value, (bool value), (const, override));
  MOCK_METHOD(void, toggle, (), (const, override));
  MOCK_METHOD(void, on, (), (const, override));
  MOCK_METHOD(void, off, (), (const, override));
  MOCK_METHOD(void, pull, (Pull), (const, override));
  MOCK_METHOD(void, irq, (uint, IRQCallback), (const, override));
  MOCK_METHOD(void, irq, (), (const, override));
};