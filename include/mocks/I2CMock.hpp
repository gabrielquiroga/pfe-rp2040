#pragma once
#include "interfaces/Communications/II2C.hpp"
#include <gmock/gmock.h>

class I2CMock : public II2C {
public:
  MOCK_METHOD(std::vector<uint8_t>, scan, (), (const, override));
  MOCK_METHOD(bool, write, (std::vector<uint8_t> data), (const, override));
  MOCK_METHOD(std::vector<uint8_t>, read, (uint8_t bytes, uint8_t registry),
              (const, override));
  MOCK_METHOD(bool, writeTo, (std::vector<uint8_t> data, uint8_t address),
              (const, override));
  MOCK_METHOD(std::vector<uint8_t>, readFrom,
              (uint8_t bytes, uint8_t registry, uint8_t address),
              (const, override));
  MOCK_METHOD(void, deinit, (), (const, override));
  MOCK_METHOD(void, setAddress, (uint8_t address), (override));
  MOCK_METHOD(int, readWord, (uint8_t registry, uint8_t size),
              (const, override));

  MOCK_METHOD(bool, writeWord, (uint8_t registry, int16_t data),
              (const, override));
  MOCK_METHOD(bool, writeWord, (uint8_t registry, int32_t data),
              (const, override));
};