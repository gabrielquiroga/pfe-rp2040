#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"

#include <stdint.h>
typedef unsigned int uint;

#ifdef NATIVE
#include <stddef.h>
typedef uint i2c_inst_t;
typedef uint uart_inst_t;
#else
typedef struct i2c_inst i2c_inst_t;
typedef struct uart_inst uart_inst_t;
#endif