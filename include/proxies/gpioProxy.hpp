#pragma once
#ifndef NATIVE
#include "pico/stdlib.h"
#else
typedef void (*gpio_irq_callback_t)(uint gpio, uint32_t events);

enum gpio_function {
  GPIO_FUNC_XIP = 0,
  GPIO_FUNC_SPI = 1,
  GPIO_FUNC_UART = 2,
  GPIO_FUNC_I2C = 3,
  GPIO_FUNC_PWM = 4,
  GPIO_FUNC_SIO = 5,
  GPIO_FUNC_PIO0 = 6,
  GPIO_FUNC_PIO1 = 7,
  GPIO_FUNC_GPCK = 8,
  GPIO_FUNC_USB = 9,
  GPIO_FUNC_NULL = 0x1f
};

void gpio_init(uint);
void gpio_set_dir(uint, bool);
void gpio_put(uint, bool);
void gpio_pull_up(uint);
void gpio_pull_down(uint);
bool gpio_get(uint);
void gpio_set_irq_enabled(uint, uint32_t, bool);
void gpio_set_irq_enabled_with_callback(uint, uint32_t, bool,
                                        gpio_irq_callback_t);

void gpio_set_function(uint, gpio_function);

void gpio_set_irq_enabled_with_callback(uint gpio, uint32_t events,
                                        bool enabled,
                                        gpio_irq_callback_t callback);

void gpio_set_irq_enabled(uint gpio, uint32_t events, bool enabled);

#endif