#pragma once

#ifndef NATIVE
#include "hardware/uart.h"
#else
void uart_init(uint *, uint);
void uart_deinit(uint *);
void uart_write_blocking(uart_inst_t *uart, const uint8_t *src, size_t len);
void uart_read_blocking(uart_inst_t *uart, uint8_t *dst, size_t len);
void uart_puts(uart_inst_t *uart, const char *s);
uint uart_is_readable(uart_inst_t *uart);
char uart_getc(uart_inst_t *uart);

void uart_set_fifo_enabled(uart_inst_t *uart, bool enabled);
void uart_set_irq_enables(uart_inst_t *uart, bool rx_has_data,
                          bool tx_needs_data);

uint uart0_value = 0;
uint uart1_value = 1;
uint *uart0 = &uart0_value;
uint *uart1 = &uart1_value;

#endif