#pragma once
#include <pico/util/queue.h>

// Inter process Queue
extern queue_t odometers_data_queue;

// Second Core Process
extern void main_core1(void);