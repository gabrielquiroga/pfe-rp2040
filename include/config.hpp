#pragma once

namespace config {

// Connection Parameters
constexpr auto rightEncoderPin = 6u;
constexpr auto leftEncoderPin = 28u;
// UART
constexpr auto baudrate = 921600u;
constexpr auto tx = 0u;
constexpr auto rx = 1u;
// MPU6500 I2C
constexpr auto I2CFrequency = 1000u * 1000u;
constexpr auto SDAPin = 4u;
constexpr auto SCLPin = 5u;

// Differential Robot
// Left Engine
constexpr auto leftEngineForwardPin = 9u;
constexpr auto leftEngineReversePin = 10u;
constexpr auto leftEngineSpeedControlPin = 11u;
// Right Engine
constexpr auto rightEngineForwardPin = 12u;
constexpr auto rightEngineReversePin = 13u;
constexpr auto rightEngineSpeedControlPin = 14u;

// Physical parameters
constexpr auto wheelRadius = 0.035f;
constexpr auto encoderStep = 18.0f * 3.1415926535897f / 180.0f;

// Core 1 Task Parameters
constexpr auto fastTimerTimeout = 1000u * 100u;
constexpr auto slowTimerTimeout = 2000u;

// Behavior Parameters
// State Machine
constexpr auto traslationTolerance = 0.03f;
constexpr auto rotationTolerance = 0.5f;

namespace pid {

// Traslation PID
namespace traslation {
constexpr auto Kp = 0.1f;
constexpr auto Ki = 0.1f;
constexpr auto Kd = 0.1f;
} // namespace traslation

// Rotation PID
namespace rotation {
constexpr auto Kp = 0.1f;
constexpr auto Ki = 0.1f;
constexpr auto Kd = 0.1f;
} // namespace rotation
} // namespace pid

} // namespace config